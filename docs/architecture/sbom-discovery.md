---
sidebar_position: 4
---

# SBOM Discovery

Hoppr supports discovery of SBOM files from the following locations:

* Local filesystem
* URL via HTTP/S GET
* As an OCI Artifact

## Local Filesystem

Example of referencing a local file:

```yaml
sboms:
  - local: sbom.json
```

## URL

The SBOM file can be stored at an HTTP/S accessible endpoint. If the file is
behind authentication, then the provided `credentials.yaml` must provide relevant
credentials.

manifest.yaml

```yaml
sboms:
  - url: http://myproject.com/artifact/1.2.3/sbom.json
```

credentials.yaml

```yaml
credential_required_services:
  - url: "myproject.com"
    user: read-user
    pass_env: read-pass/token
```

## OCI Artifact Repository

Utilizing the [oci-distribution-spec](https://github.com/opencontainers/distribution-spec/blob/main/spec.md),
it is possible to upload non-container layers to OCI registries; these are referred
to as "OCI artifacts." Hoppr has support for discovering and pulling files from
these OCI registries. The OCI has created [ORAS](https://oras.land/) to help facilitate this interactivity, and Hoppr uses this. This is useful to know, as ORAS' documentation provides information on OCI
registries that have the appropriate support for the artifacts [here](https://oras.land/implementors/).

manifest.yaml

```yaml
sboms:
  - oci:
      artifact: oci://registry.host.com/project-name/repo/path/artifact:tag
      # Optional, which platform is hosting the registraty. May be required to ensure
      # proper registry authentication. If not provided best effort is made to authenticate
      # supported options
      # - gitlab
      hoster: gitlab
```

Authentication for the registry is also managed via credentials.yaml

credentials.yaml

```yaml
credential_required_services:
  - url: "registry.host.com"
    user: read-user
    pass_env: READ_PASS_OR_TOKEN_ENV_VAR
```

### Staging OCI Artifacts

The ORAS CLI can be used to facilitate uploading artifacts to a registry. Here is a normal example command that would push an SBOM file:

```sh
# Push local sbom.json to a versioned artifact
# NOTE: The file uploaded should be in same directory as execution, else the resulting artifact will
# have a title providing additional path information which can make fetching fail due to implict path traversal
$ oras push \
    registry.gitlab.us.lmco.com:443/registry.host.com/project-name/repo/path/sbom.json:0.0.2 \
    sbom.json:application/vnd.acme.rocket.config.v1+json

# Pull artifact back
$ oras pull registry.gitlab.us.lmco.com:443/registry.host.com/project-name/repo/path/sbom.json:0.0.2
Downloading 8f20e2d27b8d sbom.json
Downloaded  8f20e2d27b8d sbom.json
Pulled registry.gitlab.us.lmco.com:443/registry.host.com/project-name/repo/path/sbom.json:0.0.2
Digest: sha256:1974323fa800f1ba23054e170f65c5b2206fcbe9349089a9c6e76530863b695b

$ ls sbom.json
sbom.json
```
