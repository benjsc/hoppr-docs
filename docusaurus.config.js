// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Hoppr',
  url: 'https://hoppr.dev',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'throw',
  favicon: 'img/Hoppr-Favicon-01.png',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'lmco', // Usually your GitHub org/user name.
  projectName: 'hoppr', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },
  stylesheets: [],
  scripts: [],

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://gitlab.com/hoppr/hoppr-docs/-/tree/main/',
        },
        // blog: {
        //   showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links
        //   editUrl:
        //    'https://gitlab.com/hoppr/hoppr-docs/-/tree/main/',
        //},
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        gtag: {
          trackingID: 'G-H1HWX8MSWJ',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      mermaid: {
        theme: {light: 'neutral', dark: 'forest'},
      },
      algolia: {
        appId: 'P3WNX5SP8Q',
        apiKey: 'aded38478e7579812cbeaa57cf2b3650',
        indexName: 'crawler_hoppr2',
        contextualSearch: true,
        // Optional: Specify domains where the navigation should occur through window.location instead on history.push. Useful when our Algolia config crawls multiple documentation sites and we want to navigate with window.location.href to them.
        externalUrlRegex: 'external\\.com|domain\\.com',
        // Optional: Algolia search parameters
        searchParameters: {},
        // Optional: path for search page that enabled by default (`false` to disable it)
        searchPagePath: 'search',
      },
      navbar: {
        title: 'Hoppr',
        logo: {
          alt: 'Hoppr logo',
          src: 'img/HopprHippo-01.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Documentation',
          },
	  {
            type: 'docsVersionDropdown',
            position: 'left',
            dropdownItemsAfter: [],
            dropdownActiveClassDisabled: true,
          },
          // {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/hoppr/hoppr',
            label: 'Hoppr GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              // {
              //   label: 'Stack Overflow',
              //   href: 'https://stackoverflow.com/questions/tagged/hoppr',
              // },
              // {
              //   label: 'Discord',
              //   href: 'https://discordapp.com/invite/hoppr',
              // },
              // {
              //   label: 'Twitter',
              //   href: 'https://twitter.com/hoppr',
              // },
            ],
          },
          {
            title: 'More',
            items: [
              // {
              //   label: 'Blog',
              //   to: '/blog',
              // },
              {
                label: 'Hoppr.dev GitLab',
                href: 'https://gitlab.com/hoppr/hoppr-docs',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} - Lockheed Martin Corporation`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
  markdown: {
    mermaid: true,
  },
  themes: ['@docusaurus/theme-mermaid'],
};

module.exports = config;
