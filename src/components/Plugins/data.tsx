type PluginItem = {
    tag: string;
    pluginType: string;
    supportedPurls: string[];
    dependencies: string[];
  };

export const PluginList: PluginItem[] = [
    { tag: "hoppr.core_plugins.tar_bundle", pluginType: "bundle", supportedPurls: [], dependencies: [] },
    { tag: "hoppr.core_plugins.oras_bundle", pluginType: "bundle", supportedPurls: [], dependencies: [] },
    { tag: "hoppr.core_plugins.collect_apt_plugin", pluginType: "collect", supportedPurls: ["deb"], dependencies: ["apt","apt-cache"] },
    { tag: "hoppr.core_plugins.composite_collector", pluginType: "collect", supportedPurls: [], dependencies: [] },
    { tag: "hoppr.core_plugins.collect_dnf_plugin", pluginType: "collect", supportedPurls: ["rpm"], dependencies: ["dnf"] },
    { tag: "hoppr.core_plugins.collect_docker_plugin", pluginType: "collect", supportedPurls: ["docker"], dependencies: ["skopeo"] },
    { tag: "hoppr.core_plugins.collect_git_plugin", pluginType: "collect", supportedPurls: ["git", "gitlab", "github"], dependencies: ["git"] },
    { tag: "hoppr.core_plugins.collect_helm_plugin", pluginType: "collect", supportedPurls: ["helm"], dependencies: ["helm"] },
    { tag: "hoppr.core_plugins.collect_maven_plugin", pluginType: "collect", supportedPurls: ["maven"], dependencies: ["mvn"] },
    { tag: "hoppr.core_plugins.collect_nexus_search", pluginType: "collect", supportedPurls: [], dependencies: [] },
    { tag: "hoppr.core_plugins.collect_pypi_plugin", pluginType: "collect", supportedPurls: ["pip", "pypi"], dependencies: [] },
    { tag: "hoppr.core_plugins.collect_raw_plugin", pluginType: "collect", supportedPurls: ["binary", "generic", "raw"], dependencies: [] },
    { tag: "hoppr.core_plugins.delta_sbom", pluginType: "process", supportedPurls: [], dependencies: [] },
];

type Dependency = {
    name: string;
    site: string;
}

export const DependencyMap: Dependency[] = [
    { name: "apt", site: "https://wiki.debian.org/Apt" },
    { name: "apt-cache", site: "https://linux.die.net/man/8/apt-cache" },
    { name: "dnf", site: "https://github.com/rpm-software-management/dnf" },
    { name: "skopeo", site: "https://github.com/containers/skopeo" },
    { name: "git", site: "https://git-scm.com/" },
    { name: "helm", site: "https://helm.sh" },
    { name: "mvn", site: "https://maven.apache.org/" },
];
